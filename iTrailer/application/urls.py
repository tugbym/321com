"""
urls.py

URL dispatch route mappings and error handlers

"""
from flask import render_template

from application import app
from application.controllers import warmup

from application.controllers import home
from application.controllers import movie
from application.controllers import profile
from application.controllers import search
from application.controllers import register
from application.controllers import login
from application.controllers import logout
from application.controllers import contact

## URL dispatch rules
# App Engine warm up handler
# See http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests
app.add_url_rule('/_ah/warmup', 'warmup', view_func=warmup.warmup)

# Home page
app.add_url_rule('/', 'home', view_func=home.view)

# Search page
app.add_url_rule('/search', 'search', view_func=search.view, methods=['POST'])

# Movie page
app.add_url_rule('/movie', 'movie', view_func=movie.view)

# Add Movie
app.add_url_rule('/movie/add', 'addmovie', view_func=movie.add, methods=['POST'])

# Profile page
app.add_url_rule('/profile/<userID>', 'profile', view_func=profile.view, methods=['GET', 'POST'])

# Edit profile page
app.add_url_rule('/profile/<userID>/edit', 'editprofile', view_func=profile.edit, methods=['GET', 'POST'])

# Delete profile page
app.add_url_rule('/profile/<userID>/delete', 'deleteprofile', view_func=profile.delete, methods=['POST'])

# Registration page
app.add_url_rule('/register', 'register', view_func=register.view)

# Add New User
app.add_url_rule('/register/add', 'adduser', view_func=register.add, methods=['POST'])

# Login page
app.add_url_rule('/login', 'login', view_func=login.view, methods=['GET', 'POST'])

# Logout page
app.add_url_rule('/logout', 'logout', view_func=logout.view)

# Contact Us page
app.add_url_rule('/contact', 'contact', view_func=contact.view)

## Error handlers
# Handle 400 errors
@app.errorhandler(400)
def bad_request(e):
    return render_template('400.html'), 400

# Handle 401 errors
@app.errorhandler(401)
def bad_request(e):
    return render_template('401.html'), 401

# Handle 404 errors
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

# Handle 405 errors
@app.errorhandler(405)
def method_not_allowed(e):
    return render_template('405.html'), 405

# Handle 500 errors
@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

