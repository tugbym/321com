"""
models.py

App Engine datastore models

"""


from google.appengine.ext import ndb
from google.appengine.api import users

class Movie(ndb.Model):
    movieName = ndb.StringProperty()
    trailerID = ndb.IntegerProperty()
    imdbID = ndb.StringProperty()
    username = ndb.StringProperty()
    userID = ndb.IntegerProperty()

class Profile(ndb.Model):
    firstName = ndb.StringProperty()
    lastName = ndb.StringProperty()
    username = ndb.StringProperty()
    password = ndb.StringProperty()
    email = ndb.StringProperty()
