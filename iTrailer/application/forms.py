"""
forms.py

Web forms based on Flask-WTForms

See: http://flask.pocoo.org/docs/patterns/wtforms/
     http://wtforms.simplecodes.com/

"""

from flaskext import wtf

from flaskext.wtf import HiddenField
from flaskext.wtf import SubmitField
from flaskext.wtf import StringField
from flaskext.wtf import PasswordField

from flaskext.wtf import validators
from flaskext.wtf import fields

class MovieForm(wtf.Form):
    movieName = HiddenField()
    trailerID = HiddenField()
    imdbID = HiddenField()
    username = HiddenField()
    userID = HiddenField()
    submit = SubmitField("Add Movie")

class SearchForm(wtf.Form):
    search = StringField("Criteria", [validators.Required(message="Please enter a value."), validators.Length(min=1, max=30, message="Your search must be between 1 and 30 characters long.")])
    submit = SubmitField("Search")
    
class RegistrationForm(wtf.Form):
    firstName = StringField("First Name", [validators.Required(message="You must enter a first name."), validators.Length(min=2, max=20, message="First Name must be between 2 and 20 characters long.")])
    lastName = StringField("Last Name", [validators.Required(message="You must enter a last name."), validators.Length(min=2, max=20, message="Last Name must be between 2 and 20 characters long.")])
    username = StringField("Username", [validators.Required(message="You must enter a username."), validators.Length(min=4, max=20, message="Username must be between 4 and 20 characters long.")])
    password = PasswordField("Password", [validators.Required(message="You must enter a password."), validators.Length(min=4, max=20, message="Password must be between 4 and 20 characters long.")])
    email = StringField("Email Address", [validators.Required(message="You must enter an email address."), validators.Length(min=5, max=30, message="Email must be between 5 and 30 characters long."), 
                                          validators.Email(message="Please enter a valid email address.")])
    submit = SubmitField("Submit")
    
class LoginForm(wtf.Form):
    username = StringField("Username", [validators.Required(message="You must enter a username."), validators.Length(min=4, max=20)])
    password = PasswordField("Password", [validators.Required(message="You must enter a password."), validators.Length(min=4, max=20)])
    submit = SubmitField("Login")