from flask import flash, render_template, request, redirect, session, abort

from ..models import Movie
from ..models import Profile

from ..forms import SearchForm
from ..forms import RegistrationForm

from Crypto.Hash import SHA256

from google.appengine.ext import ndb

def view(userID):
    
    #Get the user's profile and their movies:
    profile = Profile.get_by_id(int(userID))            
    movies = Movie.query(Movie.userID==int(userID)).fetch(10)
    
    #If we have a result from the query:
    if movies:
        return render_template('profile.html',
                               movies=movies,
                               profile=profile,
                               searchForm=SearchForm())
    
    #No results found.
    else:
        
        #If a user is logged in, set them up a base profile:
        if profile:
            return render_template('profile.html',
                                    movies=None,
                                    profile=profile,
                                    searchForm=SearchForm())
        
        #Invalid user ID:
        else:
            abort(404)
        
def edit(userID):

    #If user is authenticated:
    try:
        if session["userID"] == int(userID):
    
            #GET request:
            registrationForm = RegistrationForm(request.form)
            profile = Profile.get_by_id(int(userID))
    
            #POST request - Valid form:
            if registrationForm.validate_on_submit():
                
                #Check if new username exists already:
                query = Profile.query(Profile.username==registrationForm.username.data).fetch(1)
                
                #Username already taken.
                if query:
                    if query[0].key.id() != int(userID):
                        flash("Username taken.")
                        return render_template('editProfile.html',
                                                profile=profile,
                                                searchForm=SearchForm(),
                                                registrationForm=registrationForm), 409
                
                #No username found.
                #Hash the password with SHA:
                passhash = SHA256.new(registrationForm.password.data)
        
                #PUT changes to datastore:
                profile.firstName = registrationForm.firstName.data
                profile.lastName = registrationForm.lastName.data
                profile.username = registrationForm.username.data
                profile.password = passhash.hexdigest()
                profile.email = registrationForm.email.data
        
                profile.put()
        
                flash("Account successfully edited!")
        
                return redirect('/profile/' + userID)
    
            #Display the page on GET:
            return render_template('editProfile.html',
                                    profile=profile,
                                    searchForm=SearchForm(),
                                    registrationForm=registrationForm), 200
    
        #Invalid credentials:
        else:
            abort(401)
    
    #Invalid credentials:
    except:
        abort(401)
    
def delete(userID):
    
    #If user is authenticated:
    try:
        if session["userID"] == int(userID):
            
            #Get the user's movies:
            movies = Movie.query(Movie.userID==int(userID))

            #Filter down to the movie submitted in the form:
            imdbID = request.form.getlist('imdbID')[0]
            film = movies.filter(Movie.imdbID==imdbID).fetch(keys_only=True)
            
            #Delete movie(s) in datastore:
            ndb.delete_multi(film)
            
            flash("Movie successfully deleted!")
            
            return redirect('/profile/' + userID)
    
        #Invalid credentials:
        else:
            abort(401)
    
    #Invalid credentials:
    except:
        abort(401)