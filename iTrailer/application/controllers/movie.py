from flask import flash, request, render_template, redirect, abort

from application import app
from flask_cache import Cache

from ..forms import MovieForm
from ..forms import SearchForm

from ..models import Movie

import oauth2 as oauth
import operator
import unicodedata
import urllib
import urllib2
import json

import logging

from .. import twitter_keys
from .. import rotten_key

from google.appengine.api import urlfetch

cache = Cache(app)

def view():
    
    #Get the ID from the URL argument:
    try:
        imdbID = urllib.quote_plus(request.args.get('id'))
    
    #No argument supplied:
    except:
        abort(400)
    
    #Get the data from the cache:
    movie = cache.get(imdbID + 'info')
    trailer = cache.get(imdbID + 'trailer')
    rotten = cache.get(imdbID + 'rotten')
    
    #No cache data found:
    if not movie:
        
        #Make asynchronous URL requests:
        rpc = urlfetch.create_rpc()
        urlfetch.make_fetch_call(rpc, "http://www.myapifilms.com/imdb?idIMDB=" + imdbID + "&photos=F&actors=S")

        rpc2 = urlfetch.create_rpc()
        urlfetch.make_fetch_call(rpc2, "http://api.internetvideoarchive.com/2.0/DataService/IdMaps()?$filter=IdType%20eq%2012%20and%20ID%20eq%20%27" + str(imdbID) + "%27&$format=json&Developerid=44b9e8fe-e159-4f4c-ad86-5f35c27d174b")
        
        strippedID = imdbID.replace("tt", "")
        rpc3 = urlfetch.create_rpc()
        urlfetch.make_fetch_call(rpc3, "http://api.rottentomatoes.com/api/public/v1.0/movie_alias.json?apikey=" + rotten_key.api_key + "&type=imdb&id=" + strippedID)

        #Get the data:
        try:
            movieData = rpc.get_result()
            trailerData = rpc2.get_result()
            rottenData = rpc3.get_result()
            if movieData.status_code == 200 and trailerData.status_code == 200 and rottenData.status_code == 200:
                
                #Load the JSON representation:
                movie = json.loads(movieData.content)
                trailer = json.loads(trailerData.content)
                rotten = json.loads(rottenData.content)
                
                #Set the cache:
                cache.set(imdbID + 'info', movie, timeout=60)
                cache.set(imdbID + 'trailer', trailer, timeout=60)
                cache.set(imdbID + 'rotten', rotten, timeout=60)

        #Timed out connecting to API:
        except urlfetch.DeadlineExceededError:
            abort(500)
        
    #Get the title of the movie from the JSON:
    try:
        title = movie['title']
        
    #Couldn't find it. Return a 404 response:
    except:
        return render_template('404.html')
        
    #Get movie information from the JSON:
    year = movie['year']
    rating = movie['rated']
    releaseDate = movie['releaseDate']
    genres = movie['genres']
    directors = movie['directors']
    writers = movie['writers']
    actors = movie['actors']
    plot = movie['plot']
    imdbRating = movie['rating']
    imdbVotes = movie['votes']
    metaScore = movie['metascore']
    countries = movie['countries']
        
    #Get the IVA ID of the movie:
    try:
        ivaID = trailer['value'][0]['PublishedId']
        
    #Couldn't find it. JSON must have returned an error, so default the value to 0:
    except:
        ivaID = 0
    
    #Get the Rotten Tomatoes Ratings:
    try:
        rottenRatings = rotten['ratings']
    
    #Couldn't find it. JSON must have returned an error, so default the value to 0:
    except:
        rottenRatings = 0
    
    #Get the tweets from the cache:
    sortedTweets = cache.get(imdbID + 'tweets')
    
    #Couldn't find any:
    if not sortedTweets:
        
        #OAuth keys:
        access_token_key = twitter_keys.access_token_key
        access_token_secret = twitter_keys.access_token_secret
        consumer_key = twitter_keys.consumer_key
        consumer_secret = twitter_keys.consumer_secret
    
        _debug = 0
    
        #Create a dictionary to help find relevant tweets:
        scores = {}
        scores["movie"] = float(3)
        scores["film"] = float(3)
        scores["cinema"] = float(3)
        scores["watch"] = float(3)
        scores["theatre"] = float(2)
        scores["animation"] = float(2)
        scores["animated"] = float(2)

        sentiment_score = [ ]
        senttw = 0
        i = 0

        #Create the OAuth headers:
        oauth_token = oauth.Token(key=access_token_key, secret=access_token_secret)
        oauth_consumer = oauth.Consumer(key=consumer_key, secret=consumer_secret)
        signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()

        #Make the request:
        http_method = "GET"

        http_handler = urllib2.HTTPHandler(debuglevel=_debug)
        https_handler = urllib2.HTTPSHandler(debuglevel=_debug)
    
        url = "https://api.twitter.com/1.1/search/tweets.json?q=" +  urllib.quote_plus(title) + "&result_type=recent&lang=en&count=100"
        parameters = []
        tweets = [ ]

        req = oauth.Request.from_consumer_and_token(oauth_consumer,
                                                    token=oauth_token,
                                                    http_method=http_method,
                                                    http_url=url, 
                                                    parameters=parameters)
        req.sign_request(signature_method_hmac_sha1, oauth_consumer, oauth_token)
        headers = req.to_header()
        if http_method == "POST":
            encoded_post_data = req.to_postdata()
        else:
            encoded_post_data = None
        
        url = req.to_url()

        opener = urllib2.OpenerDirector()
        opener.add_handler(http_handler)
        opener.add_handler(https_handler)

        response = opener.open(url, encoded_post_data)
    
        #Parse the result
        for tweet in response:
            tw = json.loads(tweet.strip())
            for tw_node in tw["statuses"]:
                strdata=unicodedata.normalize("NFKD", tw_node["text"]).encode("ascii","ignore")
                terms = strdata.lower().encode('utf-8').split()
                for term in terms:
                    if scores.has_key(term):
                        senttw = senttw + scores[term]
                sentiment_score.append(senttw)
                tweets.append(strdata.lower().encode('utf-8'))
                i = i + 1
                senttw = 0
    
        #Sort result by the relevance dictionary:
        x = sorted(enumerate(sentiment_score), key=operator.itemgetter(1), reverse = True)
    
        sortedTweets = [ ]

        for i in range(0,7):
            sort = tweets[x[i][0]]
            sortedTweets.append(sort)
        
        #Set the cache:
        cache.set(imdbID + 'tweets', sortedTweets, timeout=60)
        
    return render_template('movie.html',
                          title=title,
                          year=year,
                          rating=rating,
                          directors=directors,
                          writers=writers,
                          actors=actors,
                          plot=plot,
                          genres=genres,
                          releaseDate=releaseDate,
                          imdbRating=imdbRating,
                          imdbVotes=imdbVotes,
                          metaScore=metaScore,
                          countries=countries,
                          ivaID=ivaID,
                          imdbID=imdbID,
                          rottenRatings=rottenRatings,
                          tweets=sortedTweets,
                          searchForm=SearchForm(),
                          movieForm=MovieForm()), 200

def add():
    form = MovieForm(request.form)
    
    #Movie Form was valid:
    if form.validate_on_submit():
        
        #PUT the data into the datastore:
        movieName = form.movieName.data
        trailerID = form.trailerID.data
        imdbID = form.imdbID.data
        username = form.username.data
        userID = form.userID.data
    
        movie = Movie()
        
        movie.movieName = movieName
        movie.trailerID = int(trailerID)
        movie.imdbID = imdbID
        movie.username = username
        movie.userID = int(userID)
    
        movie.put()
        
        flash('Movie successfully added!')
    
        return redirect('/movie?id=' + imdbID)
    
    #Movie Form was invalid:
    abort(400)