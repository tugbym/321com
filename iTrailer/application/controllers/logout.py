from flask import flash, session, redirect

def view():
    
    #Log the user out:
    try:
        if session["userID"]:
            session.pop('userID', None)
            session.pop('username', None)
    
            flash("You have been successfully logged out!")
        
            return redirect('/')
    
    #No user was logged in:
    except:
        return redirect('/')