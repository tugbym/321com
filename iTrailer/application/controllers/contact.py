from flask import render_template

from ..forms import SearchForm

def view():
    searchForm = SearchForm()
    
    return render_template('contact.html',
                           searchForm=searchForm), 200