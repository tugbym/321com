from flask import flash, session, request, redirect, render_template

from ..forms import LoginForm
from ..forms import SearchForm

from ..models import Profile

from Crypto.Hash import SHA256

def view():
    loginForm = LoginForm()
    
    #Login Form passed validation:
    if loginForm.validate_on_submit():
        username = loginForm.username.data
        password = loginForm.password.data
        
        #Hash the password
        passhash = SHA256.new(password).hexdigest()
        
        #See if the username/password matches:
        query = Profile.query(Profile.username==username, Profile.password==passhash).fetch(1)
        
        #Found one:
        if query:
            
            #Log the user in:
            flash("You have been successfully logged in!")
            session['userID'] = query[0].key.id()
            session['username'] = query[0].username
            
            return redirect('/')
        
        #No result:
        else:
            flash("Incorrect username and/or password.")
            return render_template('login.html',
                                   searchForm=SearchForm(),
                                   loginForm=loginForm), 400
    
    #GET request:
    return render_template('login.html',
                          searchForm=SearchForm(),
                          loginForm=loginForm), 200