from flask import render_template, abort

from application import app
from flask_cache import Cache

from ..forms import SearchForm

import urllib2
import json
import oauth2 as oauth
import unicodedata
import operator
import os

from .. import twitter_keys

from google.appengine.api import urlfetch

cache = Cache(app)

def view():    
    searchForm = SearchForm()
    
    #Attempt to get data from the cache:
    theatreData = cache.get('inTheaters')
    topMovies = cache.get('topMovies')
    
    #If not, make a request to the API's:
    if not theatreData:
    
        rpc = urlfetch.create_rpc()
        urlfetch.make_fetch_call(rpc, "http://www.myapifilms.com/imdb/inTheaters")

        rpc2 = urlfetch.create_rpc()
        urlfetch.make_fetch_call(rpc2, "http://www.myapifilms.com/imdb/top")

        #Result:
        try:
            theatre = rpc.get_result()
            top = rpc2.get_result()
            if theatre.status_code == 200 and top.status_code == 200:
                
                #Get the data:
                theatreData = json.loads(theatre.content)
                topMovies = json.loads(top.content)
                
                #Set the cache:
                cache.set('inTheaters', theatreData, timeout=60)
                cache.set('topMovies', topMovies, timeout=60)

        #Took too long to get a response:
        except urlfetch.DeadlineExceededError:
            abort(500)
    
    releaseDate = theatreData[0]["date"]    
    currentMovies = theatreData[0]["movies"]
    
    #Attempt to get tweets from cache:
    sortedTweets = cache.get('tweets')
    
    #Couldn't find any:
    if not sortedTweets:
        #OAuth keys:
        access_token_key = twitter_keys.access_token_key
        access_token_secret = twitter_keys.access_token_secret
        consumer_key = twitter_keys.consumer_key
        consumer_secret = twitter_keys.consumer_secret
    
        _debug = 0

        #Create the OAuth headers:
        oauth_token = oauth.Token(key=access_token_key, secret=access_token_secret)
        oauth_consumer = oauth.Consumer(key=consumer_key, secret=consumer_secret)
        signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()

        http_method = "GET"

        http_handler = urllib2.HTTPHandler(debuglevel=_debug)
        https_handler = urllib2.HTTPSHandler(debuglevel=_debug)
    
        url = "https://api.twitter.com/1.1/search/tweets.json?q=" + currentMovies[0]["title"] + "&result_type=recent&lang=en&count=100"
        parameters = []
        tweets = [ ]
    
        #Create a dictionary to help find relevant tweets:
        scores = {}
        scores["movie"] = float(3)
        scores["film"] = float(3)
        scores["cinema"] = float(3)
        scores["watch"] = float(3)
        scores["theatre"] = float(2)
        scores["animation"] = float(2)
        scores["animated"] = float(2)

        sentiment_score = [ ]
        senttw = 0
        i = 0

        #Make the request:
        req = oauth.Request.from_consumer_and_token(oauth_consumer,
                                                    token=oauth_token,
                                                    http_method=http_method,
                                                    http_url=url, 
                                                    parameters=parameters)
        req.sign_request(signature_method_hmac_sha1, oauth_consumer, oauth_token)
        headers = req.to_header()
        if http_method == "POST":
            encoded_post_data = req.to_postdata()
        else:
            encoded_post_data = None
        
        url = req.to_url()

        opener = urllib2.OpenerDirector()
        opener.add_handler(http_handler)
        opener.add_handler(https_handler)

        response = opener.open(url, encoded_post_data)
    
        #Parse the response
        for tweet in response:
            tw = json.loads(tweet.strip())
            for tw_node in tw["statuses"]:
                strdata=unicodedata.normalize("NFKD", tw_node["text"]).encode("ascii","ignore")
                terms = strdata.lower().encode('utf-8').split()
                for term in terms:
                    if scores.has_key(term):
                        senttw = senttw + scores[term]
                sentiment_score.append(senttw)
                tweets.append(strdata.lower().encode('utf-8'))
                i = i + 1
                senttw = 0
    
        x = sorted(enumerate(sentiment_score), key=operator.itemgetter(1), reverse = True)
    
        sortedTweets = [ ]

        for i in range(0,7):
            sort = tweets[x[i][0]]
            sortedTweets.append(sort)
        
        #Set the cache:
        cache.set('tweets', sortedTweets, timeout=60)
        
    return render_template('index.html',
                           searchForm=searchForm,
                           releaseDate=releaseDate,
                           currentMovies=currentMovies,
                           topMovies=topMovies,
                           tweets=sortedTweets), 200