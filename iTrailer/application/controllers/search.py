from flask import request, redirect, render_template

from application import app
from flask_cache import Cache

from ..forms import SearchForm

import urllib
import urllib2
import json

cache = Cache(app)

def view():
    
    searchForm = SearchForm(request.form)
    
    #Search Form valid.
    if searchForm.validate_on_submit():
        search = urllib.quote_plus(searchForm.search.data)
        
        #Attempt to get the results from the cache.
        results = cache.get(search)
        
        #Not found. Must be a new search.
        if not results:
            
            #Pass the film name into the MyAPIFilms API:
            url = urllib2.urlopen("http://www.myapifilms.com/title?title=" + search + "&limit=10&filter=M")
        
            #Get the JSON representation:
            results = json.load(url)
            url.close()
            
            try:
                if results["message"] == "Movie not found":
                    return render_template('search.html',
                                            results=0,
                                            searchForm=searchForm), 400
            
            except:
                #Set the cache.
                cache.set(search, results, timeout=60)

        return render_template('search.html',
                                results=results,
                                searchForm=searchForm), 200
    
    # Search Form not valid.
    return render_template('search.html',
                           results=0,
                           searchForm=searchForm), 400