from flask import flash, request, redirect, render_template

from ..forms import RegistrationForm
from ..forms import SearchForm

from ..models import Profile

from Crypto.Hash import SHA256

def view():
    
    #Show Registration page:
    registrationForm = RegistrationForm()
    
    return render_template('register.html',
                          searchForm=SearchForm(),
                          registrationForm=registrationForm)

def add():
    registrationForm = RegistrationForm(request.form)
    
    #Valid form:
    if registrationForm.validate_on_submit():
        profile = Profile()
        
        #See if username already exists:
        query = Profile.query(Profile.username==registrationForm.username.data).fetch(1)
        
        #Username already taken.
        if query:
            flash("Username taken.")
            return render_template('register.html',
                                    searchForm=SearchForm(),
                                    registrationForm=registrationForm), 409
        
        #Doesn't exist:
        else:
            #Hash the password with SHA:
            passhash = SHA256.new(registrationForm.password.data)
        
            #PUT new user into the datastore:
            profile.firstName = registrationForm.firstName.data
            profile.lastName = registrationForm.lastName.data
            profile.username = registrationForm.username.data
            profile.password = passhash.hexdigest()
            profile.email = registrationForm.email.data
        
            profile.put()
        
            flash("Account successfully created!")
        
            return redirect('/login')
    
    #Invalid form:
    return render_template('register.html',
                          searchForm=SearchForm(),
                          registrationForm=registrationForm), 200