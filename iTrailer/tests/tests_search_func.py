#!/usr/bin/env python
# encoding: utf-8

import os
import unittest

from google.appengine.ext import testbed

from application import app

class MovieFuncTests(unittest.TestCase):
    def setUp(self):
        
        #Configuration
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        
        #Setup the testing client
        self.app = app.test_client()
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_user_stub()
        self.testbed.init_memcache_stub()

    def tearDown(self):
        self.testbed.deactivate()
        
    def test_GET_not_allowed(self):
        response = self.app.get('/search')
        assert "Method Not Allowed" in response.data
        
    def test_search_works(self):
        response = self.app.post('/search', data=dict( search="Simpsons" ), headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Simpsons" in response.data
        
    def test_no_value_entered_error(self):
        response = self.app.post('/search', data=dict( search="" ), headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Please enter a value." in response.data
        
    def test_too_many_characters_error(self):
        response = self.app.post('/search', data=dict( search="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" ), headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Your search must be between 1 and 30 characters long." in response.data
        
    def test_no_results_found(self):
        response = self.app.post('/search', data=dict( search="fgfgfgfgvcvbcb" ), headers={"content-type": "application/x-www-form-urlencoded"})
        assert "No results found" in response.data
        
if __name__ == '__main__':
    unittest.main()