#!/usr/bin/env python
# encoding: utf-8

import os
import unittest

from google.appengine.ext import testbed

from application import app

from application.models import Profile

class MovieFuncTests(unittest.TestCase):
    def setUp(self):
        
        #Configuration
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        
        #Setup the testing client
        self.app = app.test_client()
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_user_stub()
        self.testbed.init_memcache_stub()

    def tearDown(self):
        self.testbed.deactivate()
        
    def test_register_page(self):
        response = self.app.get('/register')
        assert "First Name" in response.data
        
    def test_GET_not_allowed(self):
        response = self.app.get('/register/add')
        assert "Method Not Allowed" in response.data
        
    def test_add_new_user(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), follow_redirects=True, headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Account successfully created" in response.data
        profile = Profile.query(Profile.username == "mtugby").fetch(1)
        self.assertEqual(profile[0].username, "mtugby")
        
    def test_registration_form_validation(self):
        response = self.app.post('/register/add', data=dict( firstName="a", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        assert "First Name must be between 2 and 20 characters long." in response.data
        
    def test_valid_email(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Please enter a valid email address." in response.data
        
    def test_blank_form_entry(self):
        response = self.app.post('/register/add', data=dict( firstName="", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        assert "You must enter a first name." in response.data
        
    def test_no_two_users_with_same_name(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), follow_redirects=True, headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Username taken" in response.data
        
if __name__ == '__main__':
    unittest.main()