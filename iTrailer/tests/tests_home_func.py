#!/usr/bin/env python
# encoding: utf-8

import os
import unittest

from google.appengine.ext import testbed

from application import app

from google.appengine.api import apiproxy_stub_map
from google.appengine.api import urlfetch_stub

class MovieFuncTests(unittest.TestCase):
    def setUp(self):
        
        #Configuration
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        apiproxy_stub_map.apiproxy = apiproxy_stub_map.APIProxyStubMap() 
        apiproxy_stub_map.apiproxy.RegisterStub('urlfetch', 
        urlfetch_stub.URLFetchServiceStub())
        
        #Setup the testing client
        self.app = app.test_client()
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_user_stub()
        self.testbed.init_memcache_stub()

    def tearDown(self):
        self.testbed.deactivate()
        
    def test_home_page(self):
        response = self.app.get('/')
        assert "Tweets" in response.data
        
    def test_get_404_on_invalid_url(self):
        response = self.app.get('/dfdfd')
        assert "Page Not Found" in response.data
        
if __name__ == '__main__':
    unittest.main()