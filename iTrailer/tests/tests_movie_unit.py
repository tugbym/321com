import unittest
import urllib2
import urllib2
import json

class MovieUnitTests(unittest.TestCase):
    def setUp(self):
        self.imdbID = "tt0337978"
        self.url = urllib2.urlopen("http://www.myapifilms.com/imdb?idIMDB=" + self.imdbID + "&photos=F&actors=S")
        self.data = json.load(self.url)
        self.url.close()
        
        self.constructedUrl = "http://api.internetvideoarchive.com/2.0/DataService/IdMaps()?$filter=IdType%20eq%2012%20and%20ID%20eq%20%27" + str(self.imdbID) + "%27&$format=json&Developerid=44b9e8fe-e159-4f4c-ad86-5f35c27d174b"
        self.url = urllib2.urlopen(self.constructedUrl)
        self.trailer = json.load(self.url)
        self.url.close()
    
    def test_imdb_returns_json(self):
        self.assertEqual('Live Free or Die Hard', self.data['title'])
        
    def test_film_title_exists(self):
        self.assertTrue('title' in self.data)
        
    def test_ivaID_exists(self):
        self.assertEqual(576561, self.trailer['value'][0]['PublishedId'])
        
if __name__ == '__main__':
    unittest.main()