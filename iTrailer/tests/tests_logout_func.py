#!/usr/bin/env python
# encoding: utf-8

import os
import unittest

from google.appengine.ext import testbed

from application import app

from application.models import Profile

class MovieFuncTests(unittest.TestCase):
    def setUp(self):
        
        #Configuration
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        
        #Setup the testing client
        self.app = app.test_client()
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_user_stub()
        self.testbed.init_memcache_stub()

    def tearDown(self):
        self.testbed.deactivate()
        
    def test_logout(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/login', data=dict( username="mtugby", password="test" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.get('/logout', follow_redirects=True)
        assert "You have been successfully logged out!" in response.data
        
    def test_logout_page(self):
        response = self.app.get('/logout', follow_redirects=True)
        assert "You have been successfully logged out!" not in response.data
        
if __name__ == '__main__':
    unittest.main()