#!/usr/bin/env python
# encoding: utf-8

import os
import unittest

from google.appengine.ext import testbed

from application import app

from application.models import Profile

from werkzeug.datastructures import MultiDict

class MovieFuncTests(unittest.TestCase):
    def setUp(self):
        
        #Configuration
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        
        #Setup the testing client
        self.app = app.test_client()
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_user_stub()
        self.testbed.init_memcache_stub()

    def tearDown(self):
        self.testbed.deactivate()
        
    def test_profile_page_no_params(self):
        response = self.app.get('/profile')
        assert "Page Not Found" in response.data
        
    def test_profile_invalid_params(self):
        response = self.app.get('/profile/123')
        assert "Page Not Found" in response.data
        
    def test_valid_new_profile(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.get('/profile/' + str(query[0].key.id()))
        assert "mtugby" in response.data
        assert "Try adding some movies to your profile!" in response.data
        
    def test_movie_gets_displayed(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.post('/movie/add', data=dict( movieName="The Simpsons Movie", trailerID=int(971017), imdbID="tt0462538", username="mtugby", userID=query[0].key.id() ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.get('/profile/' + str(query[0].key.id()))
        assert "The Simpsons Movie" in response.data
        assert "Try adding some movies to your profile!" not in response.data
        
    def test_not_logged_in_has_no_access_to_edit_or_delete(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.get('/profile/' + str(query[0].key.id()))
        assert "Edit" not in response.data
        assert "X" not in response.data
        
    def test_logged_in_has_access_to_edit_or_delete(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.post('/movie/add', data=dict( movieName="The Simpsons Movie", trailerID=int(971017), imdbID="tt0462538", username="mtugby", userID=query[0].key.id() ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/login', data=dict( username="mtugby", password="test" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.get('/profile/' + str(query[0].key.id()))
        assert "Edit" in response.data
        assert "X" in response.data
        
    def test_successful_profile_edit(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.post('/login', data=dict( username="mtugby", password="test" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/profile/' + str(query[0].key.id()) + '/edit', data=dict( firstName="Mike", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), follow_redirects=True, headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Account successfully edited!" in response.data
        assert "Mike" in response.data
        
    def test_not_logged_in_has_no_access_to_edit_page(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.get('/profile/' + str(query[0].key.id()) + '/edit')
        assert "Not Authorized" in response.data
        
    def test_other_users_cannot_access_edit_page_of_other_user(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="otheruser", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/login', data=dict( username="otheruser", password="test" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.get('/profile/' + str(query[0].key.id()) + '/edit')
        assert "Not Authorized" in response.data
    
    def test_cannot_edit_to_username_already_taken(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="otheruser", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.post('/login', data=dict( username="mtugby", password="test" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/profile/' + str(query[0].key.id()) + '/edit', data=dict( firstName="Mike", lastName="Tugby", username="otheruser", password="test", email="test@example.com" ), follow_redirects=True, headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Username taken" in response.data
        assert "Account successfully edited!" not in response.data
    
    def test_successful_movie_delete(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.post('/movie/add', data=dict( movieName="The Simpsons Movie", trailerID=int(971017), imdbID="tt0462538", username="mtugby", userID=query[0].key.id() ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/login', data=dict( username="mtugby", password="test" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/profile/' + str(query[0].key.id()) + '/delete', data=MultiDict([('imdbID', 'tt0462538')]), follow_redirects=True, headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Movie successfully deleted!" in response.data
        assert "The Simpsons Movie" not in response.data
        assert "Try adding some movies to your profile!" in response.data
        
    def test_delete_does_not_accept_GET(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.get('/profile/' + str(query[0].key.id()) + '/delete')
        assert "Method Not Allowed" in response.data
        
    def test_not_logged_in_has_no_access_to_delete_page(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.post('/movie/add', data=dict( movieName="The Simpsons Movie", trailerID=int(971017), imdbID="tt0462538", username="mtugby", userID=query[0].key.id() ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/profile/' + str(query[0].key.id()) + '/delete', data=MultiDict([('imdbID', 'tt0462538')]), headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Not Authorized" in response.data
        
    def test_other_users_cannot_access_delete_page_of_other_user(self):
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="mtugby", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        query = Profile.query(Profile.username=="mtugby").fetch(1)
        response = self.app.post('/register/add', data=dict( firstName="Michael", lastName="Tugby", username="otheruser", password="test", email="test@example.com" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/login', data=dict( username="otheruser", password="test" ), headers={"content-type": "application/x-www-form-urlencoded"})
        response = self.app.post('/profile/' + str(query[0].key.id()) + '/delete', data=MultiDict([('imdbID', 'tt0462538')]), headers={"content-type": "application/x-www-form-urlencoded"})
        assert "Not Authorized" in response.data
        
if __name__ == '__main__':
    unittest.main()