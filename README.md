-![alt text](http://i57.tinypic.com/2ypj3bb.jpg "iTrailer Logo")

# iTrailer Web App

iTrailer is a movie web application created by David Okunniyi, Kenneth Ohikhuare, Marie-Ange Ndihokubwayo, Michael Tugby and Sagar Dhokiya. 
The application is a collation of movie data from across the web into one location. It currently uses the myAPIfilms, Twitter and Rotten Tomatoes API's.
Created using *Flask* and for deployment on *Google App Engine*.

## Run Locally
To install iTrailer on a local server, copy the following shell commands:

* Ensure you have installed pip and Google App Engine Python SDK. 
* You will also need Twitter and Rotten Tomatoes API keys.
* Checkout the repository: `git clone https://gitlab.com/tugbym/321com.git`
* Go into the new directory: `cd 321com`
* For CSRF protection: `python iTrailer/application/generate_keys.py`
* Install dependencies: `pip install -r iTrailer/requirements_dev.txt`
* Create twitter key file: `touch iTrailer/application/twitter_keys.py`
* Add Twitter access token, access secret, consumer key, and consumer secret in format: 
```
access_token_key = '[Enter here]'
access_token_secret = '[Enter here]'
consumer_key = '[Enter here]'
consumer_secret = '[Enter here]'
```
* Create rotten key file: `touch iTrailer/application/rotten_key.py`
* Add rotten tomatoes API key in format: 
```
api_key = '[Enter here]'
```

## Start the Server
To start a local GAE server, use the following shell command:

* `cd iTrailer`
* `dev_appserver.py --host=0.0.0.0 --admin_host=0.0.0.0 --port=3000 app.yaml`

The website can be viewed on **http://localhost:3000/**

The admin console can be viewed on **http://localhost:8000/**

## Testing
To use the in-built testing suite:

* `export FLASK_CONF=TEST`
* `python iTrailer/apptest.py [Enter App Engine SDK Path here]`

## Images
Images of the site in action:

* http://i59.tinypic.com/iz40et.png
* http://i58.tinypic.com/1zl9niw.png